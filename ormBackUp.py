#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import psycopg2
import psycopg2.extras

db = psycopg2.connect(database="postgres", 
					  user="postgres", 
					  password="2948473", 
					  host="127.0.0.1", 
					  port="5432")
cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

class Entity: 

	def __init__(self, id=None):
		self._db = db
		self._id = id
		self._cursor = cursor
		self._table = self.__class__.__name__.lower()
		self.load()	

		stream = open("in.yaml", 'r')
		out = open('out.txt', 'w')
		self._file_data = yaml.load(stream)

			
	def load(self):
		if (self._id):
			# self._table = [item for item in self._file_data.keys() if item in self.__class__.__name__]
			# self._fields = [item for key, value in self._file_data.items() for item in value.values()[0] if key in self.__class__.__name__]
			args = (self._table, self._table, self._id,)
			query = ('SELECT * FROM %s WHERE %s_id=%s')
			self._cursor.execute(query % args)
			rows = self._cursor.fetchone()
			self._rows = rows
			
			dashmark = '_'
			if (rows):
				for key, value in rows.items():
					if dashmark in key and key.split(dashmark)[1] != 'id':	
						attribute = dashmark + key.split(dashmark)[1]
						setattr(self, attribute, value)
	
	def save(self):	 
		# for row in self.__dict__.keys():
			# print row

		# for row in self._rows.keys():
			# print row

		# table = [item for item in self._file_data.keys() if item in self.__class__.__name__]

		fields = [item for key, value in self._file_data.items() for item in value.values()[0] if key in self.__class__.__name__]
		print fields

		# for item in fields:
		# 	print item
		# fields = list()

		# if (self._id):
		# 	args = (self._table, self._table, self._text, self._table, self._title, self._table, self._id, )
		# 	query = ('UPDATE %s SET %s_text=\'%s\', %s_title = \'%s\' WHERE %s_id = %s')
		# 		# query = ('UPDATE article SET article_text=\'%s\', article_title = \'%s\' WHERE article_id = %s')
		# 	# self._cursor.execute(query % args)
		# 	# self._db.commit()
		# 	print (query % args) 

		# if (self._id):
		# 	if self._table == 'category':
		# 		args = (self.title, self._id, )
		# 		query = ('UPDATE category SET category_title = \'%s\' WHERE category_id = %s')
		# 	if self._table == 'article':
		# 		args = (self._text, self._title, self._id, )
		# 		query = ('UPDATE article SET article_text=\'%s\', article_title = \'%s\' WHERE article_id = %s')
		# 	if self._table == 'tag':
		# 		args = (self._value, self._id, )
		# 		query = ('UPDATE tag SET tag_value=%s WHERE tag_id = %s')
			
		# 	self._cursor.execute(query % args)
		# 	self._db.commit()

	def __getattr__(self, attr):
		if hasattr (self, attr):
			return eval('self' + '._' + attr)
		return setattr(self, attr, None)

	def __setattr__(self, attr, value):
		self.__dict__[attr] = value

class Category(Entity):
	pass

class Article(Entity):
	pass

class Tag(Entity):
	pass

if __name__ == '__main__':

	article = Article(1)
	article.title = 'New title'
	# article.text = 'New text'
	article.save() # update article set article_title=? where article_id = ?

	# cat = Category(1)
	# cat.save()

	# article = Article(2)
	# article.title = 'Another title'
	# article.text = 'Very interesting content'
	# article.save() # update article set article_title=?, article_text=? where article_id = ?

	# category = Category(1) # select from article where article_id=?
	# print(category.title)

	# article = Article(1) # select from article where article_id=?
	# print(article.title)
	# print(article.text)
