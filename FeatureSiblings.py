#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import psycopg2
import psycopg2.extras
import itertools

db = psycopg2.connect(database="postgres", 
					  user="postgres", 
					  password="2948473", 
					  host="127.0.0.1", 
					  port="5432")
cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
stream.close()
out.close()

class Entity: 
	def __init__(self, id=None):
		self._db = db
		self._id = id
		self._cursor = cursor
		self._class = self.__class__.__name__.lower()
		self.load()	
	
	def get_data(self):
		self._fields = [item.lower() for key, value in file_data.items() 
						for item in value.values()[0] 
						if key in self.__class__.__name__]
		self._class_table = [key.lower() for key, value in file_data.items() 
						   		 for item in value.values()[1].keys()]
		self._relations_table = [item.lower() for key, value in file_data.items() 
						   		 for item in value.values()[1].keys()]
		self._relations_value = [item.lower() for key, value in file_data.items() 
						   		 for item in value.values()[1].values()]
	def load(self):
		self.get_data()
		if (self._id):
			args = (self._class, self._class, self._id,)
			query = ('SELECT * FROM %s WHERE %s_id=%s')
			self._cursor.execute(query % args)
			rows = self._cursor.fetchone()
			self._rows = rows
			self._parent = None
			self._child = None
			self._sibling = None
			if (rows):
				for key, value in rows.items():
					for item in self._fields:
						if item in key:
							setattr(self,'_' + item, value)
				for class_in_table, relation, value in itertools.izip(self._class_table, 
																  self._relations_table, 
												   				  self._relations_value):
					if value == 'one':
						if self._class == relation:
							self._child = class_in_table + 's'
						if self._class == class_in_table:	
							self._parent = relation
				for class_in_table, relation, value in itertools.izip(self._class_table, 
																  self._relations_table, 
												   				  self._relations_value):
					if value == 'many':
						if (self._class == class_in_table) and (self._child != relation + 's'):
							self._sibling = relation + 's'
							self._relation = relation

	def save(self=None):	
		if (self._id): 
			if len(self._fields) == 1:
				query = ('UPDATE %s SET %s_%s = \'%s\' WHERE %s_id = %s')
				args = (self._class, self._class, self._fields[0], \
						eval('self.' + self._fields[0]) , self._class, self._id)
			else: 
				query = ('UPDATE %s SET %s_%s = \'%s\', %s_%s = \'%s\' \
						 WHERE %s_id = %s')
			 	args = (self._class, self._class, self._fields[0], \
			 			eval('self.' + self._fields[0]),self._class, self._fields[1], \
			 			eval('self.' + self._fields[1]), self._class, self._id)
		else:
			if len(self._fields) == 1:
				args = (self._class, self._class, self._fields[0], self.title, )
				query = ('INSERT INTO %s (%s_%s) VALUES (\'%s\')')
			else:
				args = (self._class, self._class, self._fields[0], \
						self._class, self._fields[1], self.text, self.title, )
				query = ('INSERT INTO %s (%s_%s, %s_%s) VALUES (\'%s\', \'%s\')')
		self._cursor.execute(query % args)
		self._db.commit()

	def delete(self):
		args = (self._class, self._class, self._id,)
		query = ('DELETE FROM %s WHERE %s_id=%s')
		self._cursor.execute(query % args)
		self._db.commit()

	@classmethod
	def all(cls):	
		args = (cls.__name__.lower(), )
		query = ('SELECT * FROM %s ')
		cursor.execute(query % args)
		rows = cursor.fetchall()
		return [eval(cls.__name__)(row[0]) for row in rows]

	def __getattr__(self, attr):
		if attr == self._parent:
			return eval(self._parent.capitalize())(self._rows[self._parent + '_id'])
		if attr == self._child:
			return eval(self._class).all()
		if attr == self._sibling:
			args = (self._relation, self._class, self._relation, self._class,self._id)
			query = ('SELECT * FROM %s NATURAL FULL JOIN %s_%s WHERE %s_id=%s')
			self._cursor.execute(query % args)
			rows = self._cursor.fetchone()
			return [eval(self._relation.capitalize())(rows[str(self._class + '_id')])]
		if hasattr(self, '_' + attr):
			return eval('self' + '._' + attr)
		else: 
			raise AttributeError(attr)
			
	def __setattr__(self, attr, value):
		self.__dict__[attr] = value

class Category(Entity):
	pass

class Article(Entity):
	pass

class Tag(Entity):
	pass

if __name__ == '__main__':
	article = Article(1)
	print(article.text)  # select * from article where article_id=?
	print(article.category.title)  # select * from category where category_id=?
	
	category = Category(1)
	for article in category.articles: # select * from article where category_id=?
	   	print(article.title)
	
	article = Article(1)
	article.tags
	for tag in article.tags: # select * from tag natural join article_tag where article_id=?
		print(tag.value)


	# for cat in Category.all(): # select * from article
	#    print(cat.title)

	# cat = Category() # аргумент конструктора стал опциональным
	# cat.title = 'Blue Water'
	# cat.save() # insert into article (article_title) values (?)

	# article = Article()
	# article.title = 'Another title'
	# article.text = 'Very interesting content'
	# article.save() # insert into article (article_title, article_text) values (?, ?)


	# article = Article(1)
	# article.title = 'New title'
	# article.text = 'New text'
	# article.save() # update article set article_title=? where article_id = ?

	# cat = Category(1)
	# cat.title = 'Wood'
	# # print cat.title
	# cat.save()

	# article = Article(2)
	# article.title = 'Drinks'
	# article.text = 'Cola'
	# article.save() 


	# tag = Tag(2)
	# tag.value = 122
	# tag.save()


	# category = Category(1) # select from article where article_id=?
	# print(category.title)
	# print(category.text)

	# article = Article(1) # select from article where article_id=?
	# print(article.title)
	# print(article.text)
