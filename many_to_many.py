#!/usr/bin/env python

import yaml
import itertools

def create_sql(table_name, fields, date_fields):
    sql = []
    sql.append("CREATE TABLE {0} ".format(table_name))
    sql.append("({0}_id SERIAL PRIMARY KEY".format(table_name))
    for item, value in fields.items():
        sql.append(', {0}_{1} {2}'.format(table_name, item, value))
    for elements in date_fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n")
    sql.append("\n")
    return "".join(sql)

def create_function(fields):
    funct = []
    funct.append("CREATE OR REPLACE FUNCTION {0}_data() " 
                 "RETURNS TRIGGER AS $$\n".format(fields))
    funct.append("\tBEGIN\n")
    funct.append("\t\tNEW.{0} = now();\n".format(fields))
    funct.append("\t\tRETURN NEW;\n")
    funct.append("\tEND\n")
    funct.append("\t$$ language 'plpgsql';\n\n")
    return "".join(funct)

def create_trigger(tables, fields, triger_action):
    trigger = []
    trigger.append("CREATE TRIGGER {0}_data\n".format(fields))
    trigger.append("\tBEFORE {0} ON {1}\n".format(triger_action, tables))
    trigger.append("\tFOR EACH ROW\n".format(fields))
    trigger.append("\tEXECUTE PROCEDURE {0}_data();\n\n".format(fields))
    return "".join(trigger)

def create_many_to_many(table, first_tab, second_tab):
    sql = []
    sql.append("CREATE TABLE {0} ".format(table))
    sql.append("({0}_id integer REFERENCES {0}, ".format(first_tab))
    sql.append("{0}_id integer REFERENCES {0}, ".format(second_tab))
    sql.append(" PRIMARY KEY ({0}_id, {1}_id)".format(first_tab, second_tab))
    for elements in date_fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n\n")
    return "".join(sql)

def create_altertab(file_data):
    altertab = []
    siblings = []
    class_name = [key.lower() for key, value in file_data.items() 
                  for item in value.values()[1].keys()]
    class_relations = [item.lower() for key, value in file_data.items() 
                       for item in value.values()[1].keys()]
    relation_value = [item.lower() for key, value in file_data.items() 
                      for item in value.values()[1].values()]
    for name, relation, value in itertools.izip(class_name, 
                                                      class_relations, 
                                                      relation_value):
        if value == 'one':
            parent = relation
            child = name
    for name, relation, value in itertools.izip(class_name, 
                                                      class_relations, 
                                                      relation_value):
        if value == 'many':
                if name != parent: 
                    siblings.append(name)
    altertab.append("ALTER TABLE {0} ADD {1}_id INTEGER;\n".format(child, parent))
    altertab.append("ALTER TABLE {0} ADD \
                    CONSTRAINT {1}_id FOREIGN KEY ({1}_id) \
                    REFERENCES {1} ({1}_id);\n\n"
                    .format(child, parent))
    table_name = siblings[0] + '_' + siblings[1]
    altertab.append(create_many_to_many(table_name, siblings[0], siblings[1]))
    file_data.update({table_name:None})
    return "".join(altertab)

stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
date_fields = ['created', 'update']
trigger_action = ['INSERT', 'UPDATE']
for item, value in file_data.items():
    fields = value.values()[0]
    table_name = item.lower()
    sql_report = create_sql(table_name, fields, date_fields)
    out.write(sql_report)    
altertab_report = create_altertab(file_data)
out.write(altertab_report)
for field_name, action in itertools.izip(date_fields, trigger_action):
    function_report = create_function(field_name)
    out.write(function_report)
    for class_name, value in file_data.items():
        triggers_report = create_trigger(class_name.lower(), field_name, action)
        out.write(triggers_report)
stream.close()
out.close()
