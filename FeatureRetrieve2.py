#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import psycopg2
import psycopg2.extras

db = psycopg2.connect(database="postgres", 
					  user="postgres", 
					  password="2948473", 
					  host="127.0.0.1", 
					  port="5432")
cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
stream.close()
out.close()

class Entity: 

	def __init__(self, id=None):
		self._db = db
		self._id = id
		self._cursor = cursor
		self._table = self.__class__.__name__.lower()
		self.load()
			
	def load(self):
		self.load_data()
		if (self._id):
			args = (self._table, self._table, self._id,)
			query = ('SELECT * FROM %s WHERE %s_id=%s')
			self._cursor.execute(query % args)
			rows = self._cursor.fetchone()
			dashmark = '_'
			if (rows):
				for key, value in rows.items():
					if dashmark in key and key.split(dashmark)[1] != 'id':	
						attr = dashmark + key.split(dashmark)[1]
						setattr(self, attr, value)

	def load_data(self):
		self._fields = [item for key, value in file_data.items() 
						for item in value.values()[0] 
						if key in self.__class__.__name__]
		self._relations_table = [item for key, value in file_data.items() 
						   		 for item in value.values()[1].keys() 
					 	   		 if key in self.__class__.__name__]
		self._relations_value = [item for key, value in file_data.items() 
						   		 for item in value.values()[1].values() 
					 	   		 if key in self.__class__.__name__]

	def __getattr__(self, attr):	
			return eval('self' + '._' + attr)

class Category(Entity):
	pass

class Article(Entity):
	pass

class Tag(Entity):
	pass

if __name__ == '__main__':

	category = Category(1) # select from article where article_id=?
	# print(category.title)

	article = Article(1) # select from article where article_id=?
	# print(article.title)
	# print(article.text)
