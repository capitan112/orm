#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import psycopg2
import psycopg2.extras

db = psycopg2.connect(database="postgres", 
					  user="postgres", 
					  password="2948473", 
					  host="127.0.0.1", 
					  port="5432")
cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

class Entity: 
	def __init__(self, id=None):
		self._db = db
		self._id = id
		self._cursor = cursor
		self._table = self.__class__.__name__.lower()
		self.load()	
	
	def load_data(self):
		stream = open("in.yaml", 'r')
		out = open('out.txt', 'w')
		file_data = yaml.load(stream)
		self._fields = [item for key, value in file_data.items() for item in value.values()[0] if key in self.__class__.__name__]
		stream.close()
		out.close()

	def load(self):
		self.load_data()
		if (self._id):
			args = (self._table, self._table, self._id,)
			query = ('SELECT * FROM %s WHERE %s_id=%s')
			self._cursor.execute(query % args)
			rows = self._cursor.fetchone()
			self._rows = rows
			if (rows):
				for key, value in rows.items():
					for item in self._fields:
						if item in key:
							setattr(self, item, value)

	def save(self=None):	
		if (self._id): 
			if len(self._fields) == 1:
				query = ('UPDATE %s SET %s_%s = \'%s\' WHERE %s_id = %s')
				args = (self._table, self._table, self._fields[0], \
				eval('self.' + self._fields[0]) , self._table, self._id)
			else: 
				query = ('UPDATE %s SET %s_%s = \'%s\', %s_%s = \'%s\'  WHERE %s_id = %s')
			 	args = (self._table, self._table, self._fields[0], \
			 	eval('self.' + self._fields[0]),self._table, self._fields[1], \
			 	eval('self.' + self._fields[1]), self._table, self._id)
		else:
			if len(self._fields) == 1:
				args = (self._table, self._table, self._fields[0], self.title, )
				query = ('INSERT INTO %s (%s_%s) VALUES (\'%s\')')
			else:
				args = (self._table, self._table, self._fields[0], \
						self._table, self._fields[1], self.text, self.title, )
				query = ('INSERT INTO %s (%s_%s, %s_%s) VALUES (\'%s\', \'%s\')')
		self._cursor.execute(query % args)
		self._db.commit()

	def delete(self):
		args = (self._table, self._table, self._id,)
		query = ('DELETE FROM %s WHERE %s_id=%s')
		self._cursor.execute(query % args)
		self._db.commit()

	@classmethod
	def all(cls):	
		args = (cls.__name__.lower(), )
		query = ('SELECT * FROM %s ')
		cursor.execute(query % args)
		rows = cursor.fetchall()
		return [eval(cls.__name__)(row[0]) for row in rows]

	def __getattr__(self, attr):
		if hasattr (self, attr):
			return eval('self' + '._' + attr)
		return setattr(self, attr, None)

	def __setattr__(self, attr, value):
		self.__dict__[attr] = value

class Category(Entity):
	pass

class Article(Entity):
	pass

class Tag(Entity):
	pass


if __name__ == '__main__':

	for article in Article.all(): # select * from article
	   print(article.text)

	for cat in Category.all(): # select * from article
	   print(cat.title)

	# cat = Category() # аргумент конструктора стал опциональным
	# cat.title = 'Blue Water'
	# cat.save() # insert into article (article_title) values (?)

	# article = Article()
	# article.title = 'Another title'
	# article.text = 'Very interesting content'
	# article.save() # insert into article (article_title, article_text) values (?, ?)


	# article = Article(1)
	# article.title = 'New title'
	# article.text = 'New text'
	# article.save() # update article set article_title=? where article_id = ?

	# cat = Category(1)
	# cat.title = 'Wood'
	# # print cat.title
	# cat.save()

	# article = Article(2)
	# article.title = 'Drinks'
	# article.text = 'Cola'
	# article.save() # update article set article_title=?, article_text=? where article_id = ?

	# tag = Tag(2)
	# tag.value = 122
	# tag.save()


	# category = Category(1) # select from article where article_id=?
	# print(category.title)

	# article = Article(1) # select from article where article_id=?
	# print(article.title)
	# print(article.text)
